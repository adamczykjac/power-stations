# Power Stations

This is the tech task for Nordcloud recruitment process.

## Requirements

You surely needs some runtime environment, right? Here, it's only:

* [NodeJS](https://nodejs.org/).

## Running the project

As easy as running `npm start`, in your terminal.

## Contributing

There are some dev tools installed to keep the code maintainable.
Run `npm install`

> There is a `.prettierrc` file included for every team's developer code-editing convenience

Use a [GitFlow](https://www.gitflow.com/). Every branch (no, not *breath*, Sting!) you make and eventually push against the remote repo, will be picked up by the pipeline.

## Deployment

There is no actual deployment, every pipeline has a `run` stage in it which will perform the desired task. See: `.gitlab-ci.yml` for more details.
