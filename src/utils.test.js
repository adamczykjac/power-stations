const { describe } = require('mocha')
var expect = require('chai').expect // human-readable assertions
require('mocha-sinon') // needed for stubbing

const { logger } = require('./utils')

describe('Logger:', () => {
  beforeEach(function () {
    this.sinon.stub(console, 'log')
  })

  it('should output error if result has no input', function () {
    logger({})
    expect(console.log.calledOnce).to.be.true
    expect(console.log.calledWith('No input, how did you come here?')).to.be
      .true
  })

  it('should output info about no applicable station', function () {
    logger({ input: [0, 0], power: 0 })
    expect(console.log.calledOnce).to.be.true
    expect(
      console.log.calledWith('No link station within reach for point (0,0)')
    ).to.be.true
  })

  it('should output info about applicable station', function () {
    logger({ input: [0, 0], x: 10, y: 20, power: 30 })
    expect(console.log.calledOnce).to.be.true
    expect(
      console.log.calledWith(
        'Best link station for point (0,0) is station (10,20) with power: 30'
      )
    ).to.be.true
  })
})
