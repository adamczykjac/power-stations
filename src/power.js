const getPower = (reach, distance) =>
  distance > reach ? 0 : Math.pow(reach - distance, 2)

const getDistance = ({ x: ax, y: ay }, { x: bx, y: by }) => {
  const calc = Math.pow(ax - bx, 2) + Math.pow(ay - by, 2)
  return Math.sqrt(calc)
}

const getBestStation = (stations) => (input) => {
  return stations.reduce(
    (acc, station) => {
      const [x, y, reach] = station
      const power = getPower(
        reach,
        getDistance({ x: input[0], y: input[1] }, { x, y })
      )
      return acc.power > power ? acc : { input, x, y, power }
    },
    { input, power: 0 }
  )
}

module.exports = { getBestStation, getPower, getDistance }
