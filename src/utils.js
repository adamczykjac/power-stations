const logger = (res) => {
  if (!res.input || res.input[0] === undefined || res.input[1] === undefined) {
    console.log('No input, how did you come here?')
  } else {
    let msg = `No link station within reach for point (${res.input[0]},${res.input[1]})`
    if (res.power) {
      msg = `Best link station for point (${res.input[0]},${res.input[1]}) is station (${res.x},${res.y}) with power: ${res.power}`
    }
    console.log(msg)
  }
}

module.exports = { logger }
