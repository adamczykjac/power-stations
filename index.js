const { getBestStation } = require("./src/power");
const { logger: logResult } = require("./src/utils");

// inputs
const stations = [
  [0, 0, 10],
  [20, 20, 5],
  [10, 0, 12],
];

const inputPoints = [
  [0, 0],
  [100, 100],
  [15, 10],
  [18, 18],
];

// main
inputPoints.map(getBestStation(stations)).forEach(logResult);
