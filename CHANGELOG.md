# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2020-12-19)


### Features

* calculate power stations ([d7f3f2d](https://gitlab.com/adamczykjac/power-stations/commit/d7f3f2de7e3f40bdf7878c0ee07d1cd2525fc1c0))
* Gitlab pipeline setup ([0551cf6](https://gitlab.com/adamczykjac/power-stations/commit/0551cf610c8ea6f7c5d1bb35097043b4985b2999))
