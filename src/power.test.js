const { describe } = require('mocha')
var expect = require('chai').expect
require('mocha-sinon')

const { getDistance, getPower, getBestStation } = require('./power')

describe('Power:', () => {
  it('should return the Euclidan distance', () => {
    const pointA = { x: 0, y: 0 }
    const pointB = { x: 2, y: 2 }
    expect(getDistance(pointA, pointB)).to.eql(Math.sqrt(8))
  })

  it("should return the station's power", () => {
    expect(getPower(10, 5)).to.eql(25)
    expect(getPower(10, 10)).to.eql(0)
    expect(getPower(5, 10)).to.eql(0)
  })

  it('should return the best stations', function () {
    // GIVEN
    const fakeStations = [
      [0, 0, 10],
      [20, 20, 5],
      [10, 0, 12],
    ]
    // WHEN
    const result = getBestStation(fakeStations)([0, 0])
    const result2 = getBestStation(fakeStations)([10, 10])
    // THEN
    expect(result.x).to.eql(0)
    expect(result.y).to.eql(0)
    expect(result2.x).to.eql(10)
    expect(result2.y).to.eql(0)
  })
})
